# First Alliance DAO Proposals

First Alliance DAO proposals and process will be stored here.

## Create a Proposal

Please follow the steps below to submit a proposal to First Alliance DOA:

1. Create a post in the [First Alliance #dao-proposal channel](https://discord.gg/sgPqPt5hQF) outlining your proposal
2. Copy the message link from Discord (right-click / touch and hold your message, select Copy ID)
2. Visit the [First Alliance DAO on Aragon](https://client.aragon.org/#/1st/)
3. Connect your wallet
4. Navigate to Voting and Create a new vote
5. Enter a short title for your proposal and include the Discord message link for greater context
6. Submit the request, wait for the transaction to be approved
7. Post in the  [First Alliance #dao-proposal channel](https://discord.gg/sgPqPt5hQF) a link to your proposal

## Voting Process

People chat, then they vote.

## Accepting a Proposal

Once a proposal is accepted, update this repository with a new entry containing the details of the proposal.

1. Copy / paste and update an existing entry.

